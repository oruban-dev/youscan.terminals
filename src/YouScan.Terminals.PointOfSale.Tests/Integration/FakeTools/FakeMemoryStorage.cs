﻿using System.Collections.Generic;

namespace YouScan.Terminals.PointOfSale.Tests.Integration
{
    public sealed class FakeMemoryStorage
    {
        private readonly IDictionary<string, object> _fakeStorage
            = new Dictionary<string, object>();

        public void AddOrUpdate(string key, object value)
        {
            _fakeStorage[key] = value;
        }
        
        public T Get<T>(string key) where T : class
        {
            if (_fakeStorage.ContainsKey(key))
            {
                return _fakeStorage[key] as T;
            }

            return null;
        }
    }
}