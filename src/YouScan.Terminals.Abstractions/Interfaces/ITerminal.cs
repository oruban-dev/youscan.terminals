﻿namespace YouScan.Terminals.Abstractions
{
    public interface ITerminal<T, T2> : IProductLine<T>
        where T : IProductCode
        where T2 : IPriceDetails<T>
    {
        void SetPrice(T2 priceDetails);
        Invoice CalculateInvoice();
        void Reset();
    }
}