﻿using System;
using System.Collections.Generic;
using System.Linq;
using YouScan.Terminals.Abstractions;

namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public class InvoiceRulesDecorator :
        IInvoiceGenerator<ProductCode, PriceDetails>
    {
        private readonly IInvoiceGenerator<ProductCode, PriceDetails> _invoiceGenerator;

        private readonly IEnumerable<IInvoiceRule> _rules;

        public InvoiceRulesDecorator(
            IInvoiceGenerator<ProductCode, PriceDetails> invoiceGenerator,
            IEnumerable<IInvoiceRule> rules)
        {
            _invoiceGenerator = invoiceGenerator ?? throw new ArgumentNullException(nameof(invoiceGenerator));

            _rules = rules ?? Enumerable.Empty<IInvoiceRule>();
        }

        public Invoice Generate(
            IReadOnlyDictionary<ProductCode, PriceDetails> pricing,
            IReadOnlyDictionary<ProductCode, uint> products)
        {
            var rawTotal = 0M;
            var newPricing = new Dictionary<ProductCode, PriceDetails>();

            foreach (var product in products)
            {
                var initialPrice = pricing[product.Key];
                var amount = product.Value;

                rawTotal += initialPrice.UnitPrice * amount;

                var newPrice = _rules.ModifyPriceDetails(initialPrice, amount);
                newPricing.Add(product.Key, newPrice);
            }

            var invoice = _invoiceGenerator.Generate(newPricing, products);

            invoice.Total = _rules.ModifyTotal(rawTotal, invoice.Total);

            return invoice;
        }
    }
}