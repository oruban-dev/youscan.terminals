﻿using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    public sealed class DiscountCardTypeA : CumulativeTotalCard
    {
        public override int Order => 100;

        public DiscountCardTypeA(decimal discount = 0) : base(
            new UnitPriceDiscountDecorator(new PercentDiscount(discount), new InvoiceUnchangedRule()))
        {
        }
    }
}