﻿using System.Collections.Generic;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    public interface ICashierWithInvoiceRules :
        ICashier<ProductCode, PriceDetails>
    {
        List<IInvoiceRule> Rules { get; }
    }
}