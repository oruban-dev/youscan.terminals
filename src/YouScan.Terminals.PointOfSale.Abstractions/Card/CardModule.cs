﻿using System;

namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public class CardModule : ICardProvider
    {
        private readonly ICardProviderFactory _cardProviderFactory;

        public CardModule(ICardProviderFactory cardProviderFactory)
        {
            _cardProviderFactory = cardProviderFactory;
        }

        public bool CanHandle(CardNumber cardNumber)
        {
            var cardHandler = _cardProviderFactory.GetCardHandler(cardNumber);
            return cardHandler != null;
        }

        public ICard GetCardDetails(CardNumber cardNumber)
        {
            var cardHandler = _cardProviderFactory.GetCardHandler(cardNumber);
            var card = cardHandler?.GetCardDetails(cardNumber);

            return card;
        }

        public CardNumber UpdateCardDetails(ICard card)
        {
            var cardHandler = _cardProviderFactory.GetCardHandler(card.CardNumber);
            var cardNumber = cardHandler?.UpdateCardDetails(card);
            return cardNumber;
        }
    }
}