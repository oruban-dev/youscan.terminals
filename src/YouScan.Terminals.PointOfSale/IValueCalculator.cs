﻿using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    public interface IValueCalculator
    {
        decimal Calculate(PriceDetails priceDetails, uint amount);
    }
}