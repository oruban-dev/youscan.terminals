﻿using System;
using System.Collections.Generic;

namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public class CardProviderFactory : ICardProviderFactory
    {
        private readonly Dictionary<Type, ICardProvider> _handlers;

        public CardProviderFactory()
        {
            _handlers = new Dictionary<Type, ICardProvider>();
        }

        public void Register<T>(CardProvider<T> provider) where T : ICard
        {
            var key = typeof(T);
            
            _handlers[key] = provider;
        }

        public ICardProvider GetCardHandler(CardNumber cardNumber)
        {
            foreach (var handler in _handlers)
                if (handler.Value.CanHandle(cardNumber))
                    return handler.Value;

            return null;
        }
    }
}