﻿using FluentAssertions;
using Xunit;

namespace YouScan.Terminals.PointOfSale.Abstractions.Tests
{
    public class PriceListTests
    {
        [Fact]
        public void EquilityCheck()
        {
            var priceDetailsA = new PriceDetails { ProductCode = "A"};
            var priceDetailsA2 = new PriceDetails { ProductCode = "A"};
            var priceDetailsB = new PriceDetails { ProductCode = "B"};

            var priceList = new PriceList();
            var comparer = priceList.Comparer;

            comparer.Equals(null, null).Should().BeTrue();
            comparer.Equals(null, priceDetailsA).Should().BeFalse();
            comparer.Equals(priceDetailsA, null).Should().BeFalse();

            comparer.Equals(priceDetailsA, priceDetailsA).Should().BeTrue();
            comparer.Equals(priceDetailsA, priceDetailsA2).Should().BeTrue();

            comparer.Equals(priceDetailsA, priceDetailsB).Should().BeFalse();
            comparer.Equals(priceDetailsB, priceDetailsA).Should().BeFalse();
        }
    }
}