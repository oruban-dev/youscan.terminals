﻿using System;
using System.Diagnostics.CodeAnalysis;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public sealed class PointOfSaleTerminalWithCards : PointOfSaleTerminal, IPointOfSaleTerminalWithCards
    {
        private readonly ICashierWithInvoiceRules _cashier;
        private readonly ICardProvider _cardProvider;

        public PointOfSaleTerminalWithCards(
            IProductLine<ProductCode> scanner,
            ICashierWithInvoiceRules cashier,
            IInvoiceGenerator<ProductCode, PriceDetails> invoiceGenerator,
            ICardProvider cardProvider
        ) : base(scanner, cashier, invoiceGenerator)
        {
            _cashier = cashier;
            _cardProvider = cardProvider;
        }

        public void Scan(CardNumber cardNumber)
        {
            if (!_cardProvider.CanHandle(cardNumber))
            {
                throw new InvalidOperationException("Unsupported card type");
            }
            
            var card = _cardProvider.GetCardDetails(cardNumber);

            if (card is IInvoiceRule priceRule)
            {
                _cashier.Rules.Add(priceRule);
            }
        }

        public void ApplyPayment()
        {
            foreach (var rule in _cashier.Rules)
            {
                if (!(rule is ICard card)) continue;

                _cardProvider.UpdateCardDetails(card);
            }

            ResetLine();
        }

        public override void ResetLine()
        {
            _cashier.Rules.Clear();

            base.ResetLine();
        }
    }
}