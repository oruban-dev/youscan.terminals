using System.Collections.Generic;
using System.Linq;
using FluentAssertions;
using Moq;
using Xunit;
using YouScan.Terminals.Abstractions;

namespace YouScan.Terminals.PointOfSale.Abstractions.Tests
{
    public class InvoiceRulesDecoratorTests
    {
        private readonly Mock<IInvoiceGenerator<ProductCode, PriceDetails>> _mockInvoiceGenerator;
        private readonly Mock<IInvoiceRule> _mockRule;
        private readonly Invoice _invoice;

        public InvoiceRulesDecoratorTests()
        {
            _mockInvoiceGenerator = new Mock<IInvoiceGenerator<ProductCode, PriceDetails>>();
            _mockInvoiceGenerator
                .Setup(x => x.Generate(
                    It.IsAny<IReadOnlyDictionary<ProductCode, PriceDetails>>(),
                    It.IsAny<IReadOnlyDictionary<ProductCode, uint>>()))
                .Returns(new Invoice {Total = 999.99M});

            _mockRule = new Mock<IInvoiceRule>();

            _mockRule
                .Setup(x => x.ModifyPrice(It.IsAny<ModifyPriceInput>()))
                .Returns(new PriceDetails
                {
                    UnitPrice = 1,
                    DiscountPrice = 2,
                    DiscountVolume = 3,
                });

            _mockRule
                .Setup(x => x.ModifyTotal(It.IsAny<ModifyTotalInput>()))
                .Returns(123);

            _mockRule
                .Setup(x => x.Order)
                .Returns(1);

            var rules = new List<IInvoiceRule>();
            rules.Add(_mockRule.Object);

            var priceRulesDecorator
                = new InvoiceRulesDecorator(_mockInvoiceGenerator.Object, rules.OrderBy(x => x.Order));

            var pricing = new Dictionary<ProductCode, PriceDetails>()
            {
                {
                    "A", new PriceDetails
                    {
                        ProductCode = "A",
                        UnitPrice = 10,
                        DiscountPrice = 20,
                        DiscountVolume = 30
                    }
                },
                {
                    "B", new PriceDetails
                    {
                        ProductCode = "B",
                        UnitPrice = 11,
                        DiscountPrice = 22,
                        DiscountVolume = 33
                    }
                },
            };

            var products = new Dictionary<ProductCode, uint>()
            {
                {"A", 1},
                {"B", 3}
            };

            _invoice = priceRulesDecorator.Generate(pricing, products);
        }

        [Fact]
        public void DecoratedInvoiceGeneratorIsCalled()
        {
            _mockInvoiceGenerator.Verify(x => x.Generate(
                It.IsAny<IReadOnlyDictionary<ProductCode, PriceDetails>>(),
                It.IsAny<IReadOnlyDictionary<ProductCode, uint>>()), Times.Once);
        }

        [Fact]
        public void PriceModificationCalledPerProduct()
        {
            _mockRule
                .Verify(x => x.ModifyPrice(
                        It.Is<ModifyPriceInput>(s => s.Initial.ProductCode == "A")),
                    Times.Once);

            _mockRule
                .Verify(x => x.ModifyPrice(
                        It.Is<ModifyPriceInput>(s => s.Initial.ProductCode == "B")),
                    Times.Once);

            _mockRule
                .Verify(x => x.ModifyPrice(It.IsAny<ModifyPriceInput>()), Times.Exactly(2));
        }

        [Fact]
        public void TotalModificationCalledOnce()
        {
            _mockRule
                .Verify(x => x.ModifyTotal(It.IsAny<ModifyTotalInput>()),
                    Times.Once);
        }

        [Fact]
        public void PriceModificationRuleOverridesPrice()
        {
            _mockInvoiceGenerator.Verify(x => x.Generate(
                It.Is<IReadOnlyDictionary<ProductCode, PriceDetails>>( 
                    dict => dict["A"].UnitPrice == 1
                        && dict["A"].DiscountPrice == 2
                        && dict["A"].DiscountVolume == 3),
                It.IsAny<IReadOnlyDictionary<ProductCode, uint>>()), Times.Once);
        }

        [Fact]
        public void TotalModificationRuleOverridesTotal()
        {
            _invoice.Total.Should().Be(123);
        }
    }
}