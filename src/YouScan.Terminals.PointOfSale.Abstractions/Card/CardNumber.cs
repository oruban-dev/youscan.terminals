﻿namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public sealed class CardNumber
    {
        public string Code { get; set; }
    }
}