﻿using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Xunit;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;
using YouScan.Terminals.PointOfSale.Extensions;

namespace YouScan.Terminals.PointOfSale.Tests
{
    public class ServiceCollectionExtensionsTests
    {
        public class AddPointOfSaleTerminalTests
        {
            private readonly ITerminal<ProductCode, PriceDetails> _terminal;

            public AddPointOfSaleTerminalTests()
            {
                var provider = new ServiceCollection()
                    .AddPointOfSaleTerminal()
                    .BuildServiceProvider();

                _terminal = provider.GetService<ITerminal<ProductCode, PriceDetails>>();
            }

            [Fact]
            public void ServiceProviderResolvedTerminal()
            {
                _terminal.Should().NotBeNull();
            }

            [Fact]
            public void ResolvedAsPointOfSaleTerminal()
            {
                _terminal.GetType().Should().Be(typeof(PointOfSaleTerminal));
            }
        }

        public class AddTerminalWithDiscountCards
        {
            private readonly ITerminal<ProductCode, PriceDetails> _terminal;

            public AddTerminalWithDiscountCards()
            {
                var provider = new ServiceCollection()
                    .AddTerminalWithCards(s => new CardProviderFactory())
                    .BuildServiceProvider();

                _terminal = provider.GetService<IPointOfSaleTerminalWithCards>();
            }

            [Fact]
            public void ServiceProviderResolvedTerminal()
            {
                _terminal.Should().NotBeNull();
            }

            [Fact]
            public void ResolvedAsPointOfSaleTerminalWithCards()
            {
                _terminal.GetType().Should().Be(typeof(PointOfSaleTerminalWithCards));
            }
        }
    }
}