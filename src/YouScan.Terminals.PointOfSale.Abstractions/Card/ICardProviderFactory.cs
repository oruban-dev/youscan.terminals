﻿namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public interface ICardProviderFactory
    {
        void Register<T>(CardProvider<T> provider) where T : ICard;
        ICardProvider GetCardHandler(CardNumber cardNumber);
    }
}