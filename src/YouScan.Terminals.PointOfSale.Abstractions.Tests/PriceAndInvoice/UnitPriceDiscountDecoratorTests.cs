using FluentAssertions;
using Moq;
using Xunit;

namespace YouScan.Terminals.PointOfSale.Abstractions.Tests
{
    public class UnitPriceDiscountDecoratorTests
    {
        [Fact]
        public void PriceModificationInCorrectOrder()
        {
            var sequence = new MockSequence();
            
            var mockRule = new Mock<IInvoiceRule>(MockBehavior.Strict);
            mockRule
                .InSequence(sequence)
                .Setup(x => x.ModifyPrice(It.IsAny<ModifyPriceInput>()))
                .Returns(new PriceDetails());
            
            var mockDiscount = new Mock<IDiscount>(MockBehavior.Strict);
            mockDiscount
                .InSequence(sequence)
                .Setup(x => x.Apply(It.IsAny<decimal>()))
                .Returns(It.IsAny<decimal>());

            var decorator = new UnitPriceDiscountDecorator(mockDiscount.Object, mockRule.Object);
            decorator.ModifyPrice(new ModifyPriceInput());
            
            mockRule.VerifyAll();
            mockDiscount.VerifyAll();
        }
        
        [Fact]
        public void OnlyUnitPriceWasChanged()
        {
            var mockDiscount = new Mock<IDiscount>(MockBehavior.Strict);
            mockDiscount.Setup(x => x.Apply(123.45M)).Returns(120.67M);

            var mockRule = new Mock<IInvoiceRule>(MockBehavior.Strict);
            mockRule.Setup(x => x.ModifyPrice(It.IsAny<ModifyPriceInput>()))
                .Returns(new PriceDetails()
                {
                    UnitPrice = 123.45M,
                    DiscountPrice = 2,
                    DiscountVolume = 3
                });
            
            var decorator = new UnitPriceDiscountDecorator(mockDiscount.Object, mockRule.Object);

            var input = new ModifyPriceInput();

            var result = decorator.ModifyPrice(input);

            result.UnitPrice.Should().Be(120.67M);
            result.DiscountPrice.Should().Be(2);
            result.DiscountVolume.Should().Be(3);
            
            mockRule.VerifyAll();
            mockDiscount.VerifyAll();
        }
    }
}