﻿using System;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale.Tests.Integration
{
    public class CardProviderTypeA : CardProvider<DiscountCardTypeA>
    {
        private readonly FakeMemoryStorage _fakeStorage;

        public CardProviderTypeA(FakeMemoryStorage fakeStorage)
        {
            _fakeStorage = fakeStorage;
        }
        
        public override bool CanHandle(CardNumber cardNumber)
        {
            return base.CanHandle(cardNumber)
                   && true == cardNumber.Code?
                       .StartsWith("D", StringComparison.InvariantCultureIgnoreCase);
        }

        protected override DiscountCardTypeA GetCardDetails(CardNumber cardNumber)
        {
            Data data = _fakeStorage.Get<Data>(cardNumber.Code);
            if (data == null) return null;

            var discount = Math.Truncate(data.Amount / 100) * 0.01M;
            
            return new DiscountCardTypeA(discount)
            {
                Amount = data.Amount,
                CardNumber = data.CardNumber
            };
        }

        protected override CardNumber UpdateCardDetails(DiscountCardTypeA card)
        {
            var cardData = new Data
            {
                CardNumber = card.CardNumber,
                Amount = card.Amount + card.AmountToAdd
            };
            
            _fakeStorage.AddOrUpdate(card.CardNumber.Code, cardData);

            return card.CardNumber;
        }

        private class Data
        {
            public CardNumber CardNumber;
            public decimal Amount;
        }
    }
}