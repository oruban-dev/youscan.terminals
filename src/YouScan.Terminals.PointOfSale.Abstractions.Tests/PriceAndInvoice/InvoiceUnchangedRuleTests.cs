using FluentAssertions;
using Xunit;

namespace YouScan.Terminals.PointOfSale.Abstractions.Tests
{
    public class InvoiceUnchangedRuleTests
    {
        [Fact]
        public void OrderIsZero()
        {
            new InvoiceUnchangedRule().Order.Should().Be(0);
        }
        
        [Fact]
        public void PriceWasNotChanged()
        {
            var priceDetails = new PriceDetails()
            {
                UnitPrice = 1,
                DiscountPrice = 2,
                DiscountVolume = 3,
            };
            
            var input = new ModifyPriceInput()
            {
                Current = priceDetails
            };
            var result = new InvoiceUnchangedRule().ModifyPrice(input);

            result.Should().Be(priceDetails);
        }
        
        [Fact]
        public void TotalInputWasNotChanged()
        {
            var input = new ModifyTotalInput { Current = 123.45M };
            var result = new InvoiceUnchangedRule().ModifyTotal(input);
            
            result.Should().Be(123.45M);
        }
    }
}