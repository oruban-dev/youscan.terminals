﻿using System.Diagnostics.CodeAnalysis;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class PointOfSaleTerminal : Terminal<ProductCode, PriceDetails>
    {
        public PointOfSaleTerminal(
            IProductLine<ProductCode> scanner,
            ICashier<ProductCode, PriceDetails> cashier,
            IInvoiceGenerator<ProductCode, PriceDetails> invoiceGenerator
            ) : base(scanner, cashier, invoiceGenerator)
        {
        }
    }
}