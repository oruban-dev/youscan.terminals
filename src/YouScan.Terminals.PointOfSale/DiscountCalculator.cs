﻿using System;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    public class DiscountCalculator : IValueCalculator
    {
        public decimal Calculate(PriceDetails priceDetails, uint amount)
        {
            if (amount < priceDetails.DiscountVolume || priceDetails.DiscountVolume == 0)
            {
                return amount * priceDetails.UnitPrice;
            }

            var discountValue = Math.Truncate((decimal)amount / priceDetails.DiscountVolume)
                                * priceDetails.DiscountPrice;
            
            var unitValue = amount % priceDetails.DiscountVolume * priceDetails.UnitPrice;

            return discountValue + unitValue;
        }
    }
}