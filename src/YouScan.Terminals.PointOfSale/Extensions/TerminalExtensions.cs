﻿using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale.Extensions
{
    public static class TerminalExtensions
    {
        public static void SetPrice(this ITerminal<ProductCode, PriceDetails> terminal, PriceList priceList)
        {
            foreach (var priceDetails in priceList) terminal.SetPrice(priceDetails);
        }

        public static void Scan(this ITerminal<ProductCode, PriceDetails> terminal, char stringCode)
        {
            terminal.Scan(new ProductCode(stringCode.ToString()));
        }
    }
}