using FluentAssertions;
using Moq;
using Xunit;

namespace YouScan.Terminals.PointOfSale.Abstractions.Tests.Card
{
    public class CumulativeTotalCardTests
    {
        [Fact]
        public void NewCardHasEmptyAmounts()
        {
            var mockRule = new Mock<IInvoiceRule>();
            var card = new CumulativeTotalCard(mockRule.Object);
            
            card.Amount.Should().Be(0M);
            card.AmountToAdd.Should().Be(0M);
            
            mockRule.VerifyNoOtherCalls();
        }
        
        [Fact]
        public void TotalIsAccumulated()
        {
            const decimal initialTotal = 300.78M;
            const decimal modifiedTotal = 765.12M;
            const decimal cardInitialAmount = 456.56M;
            
            var mockRule = new Mock<IInvoiceRule>();
            mockRule
                .Setup(x => x.ModifyTotal(It.IsAny<ModifyTotalInput>()))
                .Returns(modifiedTotal);

            var card = new CumulativeTotalCard(mockRule.Object)
            {
                Amount = cardInitialAmount
            };

            var result = card.ModifyTotal(
                new ModifyTotalInput { Initial = initialTotal } );

            card.Amount.Should().Be(cardInitialAmount);
            card.AmountToAdd.Should().Be(initialTotal);
            
            result.Should().Be(modifiedTotal);
        }
        
        [Fact]
        public void PriceIsNotAccumulated()
        {
            var modifiedPrice = new PriceDetails()
            {
                UnitPrice = 123,
                DiscountPrice = 234,
                DiscountVolume = 345,
                ProductCode = "A"
            };
            
            const decimal cardInitialAmount = 456.56M;
            
            var mockRule = new Mock<IInvoiceRule>();
            mockRule
                .Setup(x => x.ModifyPrice(It.IsAny<ModifyPriceInput>()))
                .Returns(modifiedPrice);

            var card = new CumulativeTotalCard(mockRule.Object)
            {
                Amount = cardInitialAmount
            };

            var result = card.ModifyPrice(It.IsAny<ModifyPriceInput>());

            card.Amount.Should().Be(cardInitialAmount);
            card.AmountToAdd.Should().Be(0);
            
            result.Should().Be(modifiedPrice);
        }

        [Fact]
        public void AmountToAddIsOverriden()
        {
            var count = 1;
            var mockRule = new Mock<IInvoiceRule>();
            mockRule
                .Setup(x => x.ModifyTotal(It.IsAny<ModifyTotalInput>()))
                .Callback(() => count++)
                .Returns(count);

            var card = new CumulativeTotalCard(mockRule.Object)
            {
                Amount = 2000
            };

            card.ModifyTotal( new ModifyTotalInput { Initial = count } );
            card.ModifyTotal( new ModifyTotalInput { Initial = count } );

            count.Should().Be(3);
            card.AmountToAdd.Should().Be(2);
        }
    }
}