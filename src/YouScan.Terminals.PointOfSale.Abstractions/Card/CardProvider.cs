﻿using System;

namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public abstract class CardProvider<T> : ICardProvider
        where T : ICard
    {
        public virtual bool CanHandle(CardNumber cardNumber)
        {
            return cardNumber != null;
        }

        ICard ICardProvider.GetCardDetails(CardNumber cardNumber)
        {
            if (!CanHandle(cardNumber)) throw new InvalidOperationException("Unsupported card type");

            return GetCardDetails(cardNumber);
        }

        CardNumber ICardProvider.UpdateCardDetails(ICard card)
        {
            if (card == null) throw new ArgumentNullException(nameof(card));
            
            if (!CanHandle(card.CardNumber)) throw new InvalidOperationException("Unsupported card type");

            return UpdateCardDetails((T) card);
        }

        protected abstract T GetCardDetails(CardNumber cardNumber);

        protected abstract CardNumber UpdateCardDetails(T card);
    }
}