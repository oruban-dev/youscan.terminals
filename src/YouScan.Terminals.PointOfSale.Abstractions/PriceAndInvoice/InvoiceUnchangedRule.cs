﻿namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public class InvoiceUnchangedRule : IInvoiceRule
    {
        public int Order => 0;

        public PriceDetails ModifyPrice(ModifyPriceInput input)
        {
            return input.Current;
        }

        public decimal ModifyTotal(ModifyTotalInput input)
        {
            return input.Current;
        }
    }
}