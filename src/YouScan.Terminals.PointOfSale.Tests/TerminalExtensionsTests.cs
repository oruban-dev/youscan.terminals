﻿using Moq;
using Xunit;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;
using YouScan.Terminals.PointOfSale.Extensions;

namespace YouScan.Terminals.PointOfSale.Tests
{
    public class TerminalExtensionsTests
    {
        private readonly Mock<ITerminal<ProductCode, PriceDetails>> _terminal;

        public TerminalExtensionsTests()
        {
            _terminal = new Mock<ITerminal<ProductCode, PriceDetails>>();
        }
        
        [Fact]
        public void SetPrice()
        {
            _terminal.Object.SetPrice(new PriceList { new PriceDetails { ProductCode = "A"}});
            
            _terminal.Verify(
                x => x.SetPrice(It.Is<PriceDetails>(s=> s.ProductCode == "A")),
                Times.Once);
            
            _terminal.VerifyNoOtherCalls();
        }
        
        [Fact]
        public void Scan()
        {
            _terminal.Object.Scan('A');
            
            _terminal.Verify(
                x => x.Scan(It.Is<ProductCode>(s=> s == "A")),
                Times.Once);
            
            _terminal.VerifyNoOtherCalls();
        }
    }
}