using FluentAssertions;
using Xunit;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale.Abstractions.Tests.Card
{
    public class CardProviderTests
    {
        private readonly ICardProvider _provider;

        public CardProviderTests()
        {
            _provider = new TestCardProvider();
        }

        [Fact]
        public void CannotHandleNullCard()
        {
            _provider.CanHandle(null).Should().BeFalse();
        }

        [Fact]
        public void GetCardDetailsIsCalled()
        {
            var card = new TestCard {CardNumber = new CardNumber()};

            var details = _provider.GetCardDetails(card.CardNumber);
            details.CardNumber.Code.Should().Be("GetCardDetails");
        }

        [Fact]
        public void UpdateCardDetailsIsCalled()
        {
            var card = new TestCard {CardNumber = new CardNumber()};

            var cardNumber = _provider.UpdateCardDetails(card);
            cardNumber.Code.Should().Be("UpdateCardDetails");
        }
    }
}

public class TestCard : ICard
{
    public CardNumber CardNumber { get; set; }
}

public class TestCardProvider : CardProvider<TestCard>
{
    protected override TestCard GetCardDetails(CardNumber cardNumber)
    {
        return new TestCard
        {
            CardNumber = new CardNumber {Code = "GetCardDetails"}
        };
    }

    protected override CardNumber UpdateCardDetails(TestCard card)
    {
        return new CardNumber {Code = "UpdateCardDetails"};
    }
}
