using FluentAssertions;
using Xunit;

namespace YouScan.Terminals.PointOfSale.Abstractions.Tests
{
    public class ProductCodeTests
    {
        // ReSharper disable once InconsistentNaming
        private readonly ProductCode codeX = new ProductCode("A");
        // ReSharper disable once InconsistentNaming
        private readonly ProductCode codeY = new ProductCode("A");
        // ReSharper disable once InconsistentNaming
        private readonly ProductCode codeZ = new ProductCode("B");

        [Fact]
        public void Method_Equals()
        {
            codeX.Equals(null).Should().BeFalse();
            codeX.Equals(new object()).Should().BeFalse();
            
            codeX.Equals(codeY).Should().BeTrue();
            codeX.Equals(codeZ).Should().BeFalse();
        }

        [Fact]
        public void Method_GetHashCode()
        {
            codeX.GetHashCode().Should().Be(codeY.GetHashCode());
            codeX.GetHashCode().Should().NotBe(codeZ.GetHashCode());
        }

        [Fact]
        public void Operators_Equality()
        {
            (null == codeX).Should().BeFalse();
            (codeX == null).Should().BeFalse();
            
            (codeX == codeY).Should().BeTrue();
            (codeX == codeZ).Should().BeFalse();
        }

        [Fact]
        public void Operators_Inequality()
        {
            (null != codeX).Should().BeTrue();
            (codeX != null).Should().BeTrue();
            
            (codeX != codeY).Should().BeFalse();
            (codeX != codeZ).Should().BeTrue();
        }

        [Fact]
        public void Method_ToString()
        {
            codeX.ToString().Should().Be("Code: A");
            codeZ.ToString().Should().Be("Code: B");
        }
    }
}