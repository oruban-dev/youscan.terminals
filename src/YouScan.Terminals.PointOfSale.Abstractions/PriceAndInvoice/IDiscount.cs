﻿namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public interface IDiscount
    {
        decimal Apply(decimal price);
    }
}