using System.Collections.Generic;
using FluentAssertions;
using Moq;
using Xunit;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale.Tests
{
    public class CashierTests
    {
        private readonly IReadOnlyList<ProductCode> _knownProducts;
        private readonly Cashier _cashier;
            
        public CashierTests()
        {
            _cashier = new Cashier();
            
            _knownProducts = new List<ProductCode>()
            {
                "A",
                "B",
                "C",
                "D"
            };

            foreach (var productCode in _knownProducts)
            {
                _cashier.SetPrice(new PriceDetails { ProductCode = productCode });
            }
        }

        [Fact]
        public void NoPricesAreReturnedForUnknownProduct()
        {
            bool result = _cashier.HasPrice("unknownCode");
            result.Should().BeFalse();
        }
        
        [Fact]
        public void PricesAreReturnedForKnowProduct()
        {
            bool result = _cashier.HasPrice(_knownProducts[0]);
            result.Should().BeTrue();
        }
        
        [Fact]
        public void ResetRemoveAllPrices()
        {
            _cashier.Reset();
            bool result = _cashier.HasPrice(_knownProducts[0]);
            result.Should().BeFalse();
        }
        
        [Fact]
        public void InvoiceGeneratorIsCalled()
        {
            var noProducts = new Dictionary<ProductCode, uint>();

            var invoiceGenerator = new Mock<IInvoiceGenerator<ProductCode, PriceDetails>>();
            
            _cashier.CalculateInvoice(noProducts, invoiceGenerator.Object);
            
            invoiceGenerator.Verify(x => x.Generate(
                It.IsAny<IReadOnlyDictionary<ProductCode, PriceDetails>>(),
                It.IsAny<IReadOnlyDictionary<ProductCode, uint>>()));
            
            invoiceGenerator.VerifyNoOtherCalls();
        }
    }
}