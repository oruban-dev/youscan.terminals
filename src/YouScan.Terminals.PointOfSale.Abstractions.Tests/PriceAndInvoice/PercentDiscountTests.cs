using System;
using FluentAssertions;
using Xunit;

namespace YouScan.Terminals.PointOfSale.Abstractions.Tests
{
    public class PercentDiscountTests
    {
        [Theory]
        [InlineData(123.65, 0, 123.65)]
        [InlineData(123.65, 0.01, 122.4135)]
        [InlineData(123.65, 0.5, 61.825)]
        [InlineData(123.65, 0.99, 1.2365)]
        [InlineData(123.65, 1, 0)]
        public void DiscountAppliesCorrectly(
            decimal price,
            decimal discountPercent,
            decimal expectedPrice)
        {
            var discount = new PercentDiscount(discountPercent);
            var newPrice = discount.Apply(price);
            newPrice.Should().Be(expectedPrice);
        }

        [Theory]
        [InlineData(-0.01)]
        [InlineData(1.01)]
        public void InvalidDiscount_ThrowException(decimal invalidPercent)
        {
            Assert.Throws<ArgumentException>(() => new PercentDiscount(invalidPercent));
        }
    }
}