﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    public class CashierWithInvoiceRules : Cashier, ICashierWithInvoiceRules
    {
        public List<IInvoiceRule> Rules { get; }

        public CashierWithInvoiceRules()
        {
            Rules = new List<IInvoiceRule>();
        }

        public override Invoice CalculateInvoice(
            IReadOnlyDictionary<ProductCode, uint> products,
            IInvoiceGenerator<ProductCode, PriceDetails> invoiceGenerator)
        {
            var invoiceGeneratorWithRules = new InvoiceRulesDecorator(invoiceGenerator, Rules);

            var invoice = base.CalculateInvoice(products, invoiceGeneratorWithRules);
            invoice.Seal();

            return invoice;
        }

        public override void Reset()
        {
            Rules.Clear();
            base.Reset();
        }
    }
}