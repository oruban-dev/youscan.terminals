﻿using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;
using YouScan.Terminals.PointOfSale.Extensions;

namespace YouScan.Terminals.PointOfSale.Tests.Integration
{
    [Trait("Category","Integration")]
    public class PointOfSaleTerminalTests
    {
        private readonly ITerminal<ProductCode, PriceDetails> _terminal;

        public PointOfSaleTerminalTests()
        {
            _terminal = new ServiceCollection()
                .AddPointOfSaleTerminal()
                .BuildServiceProvider()
                .GetService<ITerminal<ProductCode, PriceDetails>>();

            SetupPrices();
        }

        [Theory]
        [InlineData("", 0)]
        [InlineData("ABCDABA", 13.25)]
        [InlineData("CCCCCCC", 6)]
        [InlineData("ABCD", 7.25)]
        public void InvoiceCalculationCorrect(string codesString, decimal expectedTotal)
        {
            foreach (char code in codesString)
            {
                _terminal.Scan(code);
            }

            var invoice = _terminal.CalculateInvoice();

            invoice.Total.Should().Be(expectedTotal);
        }

        private void SetupPrices()
        {
            var priceList = new PriceList
            {
                new PriceDetails
                {
                    ProductCode = "A",
                    UnitPrice = 1.25M,
                    DiscountVolume = 3,
                    DiscountPrice = 3M
                },
                new PriceDetails
                {
                    ProductCode = "B",
                    UnitPrice = 4.25M
                },
                new PriceDetails
                {
                    ProductCode = "C",
                    UnitPrice = 1M,
                    DiscountVolume = 6,
                    DiscountPrice = 5M
                },
                new PriceDetails
                {
                    ProductCode = "D",
                    UnitPrice = 0.75M
                },
            };

            _terminal.SetPrice(priceList);
        }
    }
}