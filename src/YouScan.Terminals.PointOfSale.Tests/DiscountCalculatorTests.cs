﻿using FluentAssertions;
using Xunit;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale.Tests
{
    public class DiscountCalculatorTests
    {
        [Theory]
        [InlineData(0, 0, 0)]
        [InlineData(5, 0, 0)]
        [InlineData(5, 1, 5)]
        [InlineData(5, 2, 10)]
        public void UnitPriceCalculation(decimal unitPrice, uint amount, decimal expectedTotal)
        {
            var priceDetails = new PriceDetails()
            {
                UnitPrice = unitPrice
            };
            
            var result = new DiscountCalculator().Calculate(priceDetails, amount);

            result.Should().Be(expectedTotal);
        }
        
        [Theory]
        [InlineData(5, 0, 0.06)]
        [InlineData(5, 1, 30)]
        [InlineData(5, 2, 15)]
        [InlineData(5, 3, 10)]
        [InlineData(5, 4, 5.02)]
        [InlineData(5, 5, 5.01)]
        [InlineData(5, 6, 5)]
        [InlineData(5, 7, 0.06)]
        [InlineData(3, 4, 3.02)]
        [InlineData(3, 6, 3)]
        public void DiscountPriceCalculation(
            decimal discountPrice,
            uint discountVolume,
            decimal expectedTotal)
        {
            var priceDetails = new PriceDetails()
            {
                UnitPrice = 0.01M,
                DiscountPrice = discountPrice,
                DiscountVolume = discountVolume
            };
            
            const uint amount = 6;
                
            var result = new DiscountCalculator().Calculate(priceDetails, amount);

            result.Should().Be(expectedTotal);
        }
    }
}