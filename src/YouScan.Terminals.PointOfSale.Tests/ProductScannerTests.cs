﻿using FluentAssertions;
using Xunit;

namespace YouScan.Terminals.PointOfSale.Tests
{
    public class ProductScannerTests
    {
        private readonly ProductScanner _scanner;

        public ProductScannerTests()
        {
            _scanner = new ProductScanner();
        }

        [Fact]
        public void ResetNonEmptyLine()
        {
            _scanner.Scan("A");

            _scanner.GetProducts().Should().HaveCountGreaterThan(0);
            _scanner.ResetLine();

            _scanner.GetProducts().Should().BeEmpty();
        }

        [Fact]
        public void ResetEmptyLine_NoExceptions()
        {
            _scanner.ResetLine();
        }

        [Fact]
        public void ResetResetedLine_NoExceptions()
        {
            _scanner.ResetLine();
            _scanner.ResetLine();
        }

        [Fact]
        public void ScanWorksAfterReset()
        {
            _scanner.ResetLine();
            _scanner.Scan("A");

            _scanner.GetProducts().Should().HaveCountGreaterThan(0);
        }

        [Fact]
        public void CumulativeScan()
        {
            _scanner.Scan("A");
            _scanner.Scan("A");
            _scanner.Scan("A");

            var scannedProducts = _scanner.GetProducts();
            
            scannedProducts.Should().HaveCount(1);
            scannedProducts.Should()
                .OnlyContain(x => x.Key == "A" && x.Value == 3);
        }
        
        [Fact]
        public void ScanDifferentProducts()
        {
            _scanner.Scan("A");
            _scanner.Scan("B");
            
            var scannedProducts = _scanner.GetProducts();

            scannedProducts.Should().HaveCount(2);
            
            scannedProducts.Should()
                .ContainSingle(x => x.Key == "A" && x.Value == 1);
            
            scannedProducts.Should()
                .ContainSingle(x => x.Key == "B" && x.Value == 1);
        }
    }
}