﻿using System.Collections.Generic;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    public class Cashier : ICashier<ProductCode, PriceDetails>
    {
        private readonly Dictionary<ProductCode, PriceDetails> _pricing;

        public Cashier()
        {
            _pricing = new Dictionary<ProductCode, PriceDetails>();
        }

        public bool HasPrice(ProductCode productCode)
        {
            return _pricing.ContainsKey(productCode);
        }

        public void SetPrice(PriceDetails priceDetails)
        {
            _pricing[priceDetails.ProductCode] = priceDetails;
        }

        public virtual Invoice CalculateInvoice(
            IReadOnlyDictionary<ProductCode, uint> products,
            IInvoiceGenerator<ProductCode, PriceDetails> invoiceGenerator)
        {
            var invoice = invoiceGenerator.Generate(_pricing, products);
            invoice?.Seal();
            
            return invoice;
        }

        public virtual void Reset()
        {
            _pricing.Clear();
        }
    }
}