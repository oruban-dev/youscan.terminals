using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using YouScan.Terminals.PointOfSale.Abstractions;
using YouScan.Terminals.PointOfSale.Extensions;

namespace YouScan.Terminals.PointOfSale.Tests.Integration
{
    public static class Extensions
    {
        public static IServiceCollection AddTerminalWithDiscountCards(this IServiceCollection services)
        {
            ICardProviderFactory CardProviderFactoryCreator(IServiceProvider provider)
            {
                var fakeStorage = provider.GetService<FakeMemoryStorage>();
                
                var cardProvider = new CardProviderTypeA(fakeStorage);

                var factory = new CardProviderFactory();
                factory.Register(cardProvider);
                
                return factory;
            }
            
            services.TryAddSingleton<FakeMemoryStorage>();

            services.AddTerminalWithCards(CardProviderFactoryCreator);
            
            return services;
        }
    }
}