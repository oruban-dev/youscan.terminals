# Terminals

AppVeyor: [![Build status](https://ci.appveyor.com/api/projects/status/7593iatassbp7wto?svg=true)](https://ci.appveyor.com/project/OleksiiRuban/youscan-terminals)

## License

This project is licensed under the **Apache License**. This means that you can use, modify and distribute it freely. See [http://www.apache.org/licenses/LICENSE-2.0.html](http://www.apache.org/licenses/LICENSE-2.0.html) for more details.