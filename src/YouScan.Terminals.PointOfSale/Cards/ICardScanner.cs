﻿using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    public interface ICardScanner
    {
        void Scan(CardNumber cardNumber);
    }
}