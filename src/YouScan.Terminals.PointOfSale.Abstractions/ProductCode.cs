﻿using System.Diagnostics;
using YouScan.Terminals.Abstractions;

namespace YouScan.Terminals.PointOfSale.Abstractions
{
    [DebuggerDisplay("{" + nameof(Value) + "}")]
    public class ProductCode : IProductCode
    {
        public string Value { get; }

        public ProductCode(string value)
        {
            Value = value;
        }

        public override bool Equals(object other)
        {
            if (other == null) return false;

            if (other.GetType() != GetType()) return false;

            return ((ProductCode) other).Value == Value;
        }

        public bool Equals(string other)
        {
            return Value == other;
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public override string ToString()
        {
            return $"Code: {Value}";
        }
        
        #region Override Operators
        
        public static bool operator == (ProductCode x, ProductCode y)
        {
            if (ReferenceEquals(x, null))
            {
                return ReferenceEquals(y, null);
            }

            return x.Equals(y);
        }

        public static bool operator != (ProductCode x, ProductCode y)
        {
            return !(x == y);
        }
        
        public static implicit operator ProductCode(string str)
        {
            return new ProductCode(str);
        }
        
        #endregion Override Operators
    }
}