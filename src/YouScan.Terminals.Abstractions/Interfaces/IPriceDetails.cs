﻿namespace YouScan.Terminals.Abstractions
{
    public interface IPriceDetails<T>
        where T : IProductCode
    {
        T ProductCode { get; }
    }
}