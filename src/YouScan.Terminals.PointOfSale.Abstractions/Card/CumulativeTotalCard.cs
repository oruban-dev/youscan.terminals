﻿using System;

namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public class CumulativeTotalCard : ICard, IInvoiceRule
    {
        private readonly IInvoiceRule _rule;

        public decimal Amount { get; set; }
        public decimal AmountToAdd { get; private set; }

        public CardNumber CardNumber { get; set; }

        public virtual int Order => _rule.Order;

        public CumulativeTotalCard(IInvoiceRule rule)
        {
            _rule = rule ?? throw new ArgumentNullException(nameof(rule));
        }
        
        public PriceDetails ModifyPrice(ModifyPriceInput input)
        {
            return _rule.ModifyPrice(input);
        }

        public decimal ModifyTotal(ModifyTotalInput input)
        {
            var total = _rule.ModifyTotal(input);
            AmountToAdd = input.Initial;
            return total;
        }
    }
}