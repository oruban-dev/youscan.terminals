﻿using System;
using System.Collections.Generic;

namespace YouScan.Terminals.Abstractions
{
    public class Terminal<T, T2> : ITerminal<T, T2> 
        where T : IProductCode
        where T2 : IPriceDetails<T>
    {
        private readonly IProductLine<T> _scanner;
        private readonly ICashier<T, T2> _cashier;
        private readonly IInvoiceGenerator<T, T2> _invoiceGenerator;

        public Terminal(
            IProductLine<T> scanner,
            ICashier<T, T2> cashier,
            IInvoiceGenerator<T, T2> invoiceGenerator)
        {
            _scanner = scanner ?? throw new ArgumentNullException(nameof(scanner));
            _cashier = cashier ?? throw new ArgumentNullException(nameof(cashier));
            _invoiceGenerator = invoiceGenerator ?? throw new ArgumentNullException(nameof(invoiceGenerator));
        }

        public void SetPrice(T2 priceDetails)
        {
            if (priceDetails == null) throw new ArgumentNullException(nameof(priceDetails));

            _cashier.SetPrice(priceDetails);
        }

        public void Scan(T productCode)
        {
            if (productCode == null) throw new ArgumentNullException(nameof(productCode));

            if (!_cashier.HasPrice(productCode))
            {
                throw new InvalidOperationException($"Scan failed for undefined product: {productCode}" );
            }
            
            _scanner.Scan(productCode);
        }

        public Invoice CalculateInvoice()
        {
            var products = _scanner.GetProducts();

            return _cashier.CalculateInvoice(products, _invoiceGenerator);
        }

        public virtual void ResetLine()
        {
            _scanner.ResetLine();
        }

        public virtual void Reset()
        {
            _scanner.ResetLine();
            _cashier.Reset();
        }

        IReadOnlyDictionary<T, uint> IProductLine<T>.GetProducts()
        {
            return _scanner.GetProducts();
        }
    }
}