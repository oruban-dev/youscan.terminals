﻿using System;
using FluentAssertions;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using YouScan.Terminals.PointOfSale.Abstractions;
using YouScan.Terminals.PointOfSale.Extensions;

namespace YouScan.Terminals.PointOfSale.Tests.Integration
{
    [Trait("Category","Integration")]
    public class PointOfSaleTerminalWithCardsTests
    {
        private readonly IPointOfSaleTerminalWithCards _pointOfSaleTerminal;
        private readonly ICardProvider _cardProvider;

        public PointOfSaleTerminalWithCardsTests()
        {
            IServiceProvider provider = new ServiceCollection()
                .AddTerminalWithDiscountCards()
                .BuildServiceProvider();

            var storage = provider.GetService<FakeMemoryStorage>();

            _cardProvider = new CardProviderTypeA(storage);
            
            _pointOfSaleTerminal = provider.GetService<IPointOfSaleTerminalWithCards>();
            SetupPrices();
        }

        [Theory]
        [InlineData("", 0)]
        [InlineData("ABCDABA", 13.25)]
        [InlineData("CCCCCCC", 6)]
        [InlineData("ABCD", 7.25)]
        public void CaseWithoutCard(string codesString, decimal expectedTotal)
        {
            foreach (char code in codesString)
            {
                _pointOfSaleTerminal.Scan(code);
            }

            var invoice = _pointOfSaleTerminal.CalculateInvoice();

            invoice.Total.Should().Be(expectedTotal);
        }
        
        [Theory]
        [InlineData("", 0.3, 0, 0)]
        [InlineData("ABCDABA", 0.3, 10.175, 14)]
        [InlineData("CCCCCCC", 0.3, 5.7, 7)]
        [InlineData("ABCD", 0.3, 5.075, 7.25)]
        public void CaseWithDiscountCard(
            string codesString,
            decimal discount,
            decimal expectedTotal,
            decimal expectedAmountAddedOnCard)
        {
            foreach (char code in codesString)
            {
                _pointOfSaleTerminal.Scan(code);
            }

            var card = SetupDiscountCard(discount);

            _pointOfSaleTerminal.Scan(card.CardNumber);

            var invoice = _pointOfSaleTerminal.CalculateInvoice();
            invoice.Total.Should().Be(expectedTotal);
            
            _pointOfSaleTerminal.ApplyPayment();

            var updatedCard = (DiscountCardTypeA) _cardProvider.GetCardDetails(card.CardNumber);
            updatedCard.Amount.Should().Be(card.Amount + expectedAmountAddedOnCard);
        }

        private DiscountCardTypeA SetupDiscountCard(decimal discount)
        {
            //setup amount to get desired discount
            var amount = discount * 10000;
                
            var card = new DiscountCardTypeA(discount)
            {
                CardNumber = new CardNumber {Code = "D0707"},
                Amount = amount
            };

            _cardProvider.UpdateCardDetails(card);
            
            return card;
        }

        private void SetupPrices()
        {
            var priceList = new PriceList
            {
                new PriceDetails
                {
                    ProductCode = "A",
                    UnitPrice = 1.25M,
                    DiscountVolume = 3,
                    DiscountPrice = 3M
                },
                new PriceDetails
                {
                    ProductCode = "B",
                    UnitPrice = 4.25M
                },
                new PriceDetails
                {
                    ProductCode = "C",
                    UnitPrice = 1M,
                    DiscountVolume = 6,
                    DiscountPrice = 5M
                },
                new PriceDetails
                {
                    ProductCode = "D",
                    UnitPrice = 0.75M
                },
            };

            _pointOfSaleTerminal.SetPrice(priceList);
        }
    }
}