﻿using System.Collections.Generic;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    public class ProductScanner : IProductLine<ProductCode>
    {
        private readonly Dictionary<ProductCode, uint> _lineState = new Dictionary<ProductCode, uint>();

        public void Scan(ProductCode productCode)
        {
            if (_lineState.ContainsKey(productCode))
            {
                _lineState[productCode]++;
            }
            else
            {
                _lineState.Add(productCode, 1);
            }
        }

        public IReadOnlyDictionary<ProductCode, uint> GetProducts()
        {
            return _lineState;
        }

        public void ResetLine()
        {
            _lineState.Clear();
        }
    }
}