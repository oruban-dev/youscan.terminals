﻿using System.Diagnostics;
using YouScan.Terminals.Abstractions;

namespace YouScan.Terminals.PointOfSale.Abstractions
{
    [DebuggerDisplay("{ProductCode}. {UnitPrice}|{DiscountPrice}|{DiscountVolume}")]
    public class PriceDetails : IPriceDetails<ProductCode>
    {
        public decimal UnitPrice { get; set; }
        public decimal DiscountPrice { get; set; }
        public uint DiscountVolume { get; set; }
        public ProductCode ProductCode { get; set; }
    }
}