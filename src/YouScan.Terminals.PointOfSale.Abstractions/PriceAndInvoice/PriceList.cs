﻿using System.Collections.Generic;

namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public class PriceList : HashSet<PriceDetails>
    {
        public PriceList() : base(new PriceDetailsEqualityComparer())
        {
        }

        private class PriceDetailsEqualityComparer : IEqualityComparer<PriceDetails>
        {
            public bool Equals(PriceDetails x, PriceDetails y)
            {
                if (x == null && y == null) return true;
                if (x == null || y == null) return false;

                return x.ProductCode.Equals(y.ProductCode);
            }

            public int GetHashCode(PriceDetails obj)
            {
                return obj.ProductCode.GetHashCode();
            }
        }
    }
}