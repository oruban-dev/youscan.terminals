﻿using System;

namespace YouScan.Terminals.Abstractions
{
    public class Invoice
    {
        private bool _sealedFlag;
        private decimal _total;

        public Invoice()
        {
        }

        [Obsolete("Use default ctor and call Seal method to do it unchangeable")]
        public Invoice(decimal total)
        {
            Total = total;
            Seal();
        }

        public bool IsSealed
        {
            get => _sealedFlag;
        }

        public decimal Total
        {
            get => _total;
            set
            {
                if (_sealedFlag == true)
                {
                    throw new InvalidOperationException("Invoice is sealed");
                }

                _total = value;
            }
        }

        public void Seal()
        {
            _sealedFlag = true;
        }
    }
}