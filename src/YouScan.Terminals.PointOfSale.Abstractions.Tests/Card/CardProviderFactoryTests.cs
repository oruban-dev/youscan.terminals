using FluentAssertions;
using Moq;
using Xunit;

namespace YouScan.Terminals.PointOfSale.Abstractions.Tests.Card
{
    public class CardProviderFactoryTests
    {
        [Fact]
        public void CardProviderFactory()
        {
            var factory = new CardProviderFactory();

            var mockProvider = new Mock<CardProvider<ICard>>();
            mockProvider
                .Setup(x => x.CanHandle(It.Is<CardNumber>(s => s.Code == "D01")))
                .Returns(true);
            
            var cardHandler = factory.GetCardHandler(new CardNumber() {Code = "D01"});
            cardHandler.Should().BeNull();
            
            factory.Register(mockProvider.Object);
            
            cardHandler = factory.GetCardHandler(new CardNumber() {Code = "D01"});
            cardHandler.Should().NotBeNull();

            mockProvider
                .Verify(x => x.CanHandle(It.Is<CardNumber>(s => s.Code == "D01")),
                    Times.Once);
        }
    }
}