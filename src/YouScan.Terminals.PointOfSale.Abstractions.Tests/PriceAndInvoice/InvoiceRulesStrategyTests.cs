using System.Collections.Generic;
using FluentAssertions;
using Moq;
using Xunit;

namespace YouScan.Terminals.PointOfSale.Abstractions.Tests
{
    public class InvoiceRulesStrategyTests
    {
        private Mock<IInvoiceRule> _mockRule;
        private Mock<IInvoiceRule> _mockRule2;

        public InvoiceRulesStrategyTests()
        {
            _mockRule = new Mock<IInvoiceRule>(MockBehavior.Strict);

            _mockRule
                .Setup(x => x.Order)
                .Returns(2);
            
            _mockRule2 = new Mock<IInvoiceRule>(MockBehavior.Strict);

            _mockRule2
                .Setup(x => x.Order)
                .Returns(1);
        }
        
        [Fact]
        public void ModifyPriceDetails()
        {
            var priceDetailsRule1 = new PriceDetails
            {
                UnitPrice = 1,
                DiscountPrice = 2,
                DiscountVolume = 3,
            };
            
            var priceDetailsRule2 = new PriceDetails
            {
                UnitPrice = 10,
                DiscountPrice = 20,
                DiscountVolume = 30,
            };
            
            var sequence = new MockSequence();
            
            _mockRule
                .InSequence(sequence)
                .Setup(x => x.ModifyPrice(It.IsAny<ModifyPriceInput>()))
                .Returns(priceDetailsRule1);
            
            _mockRule2
                .InSequence(sequence)
                .Setup(x => x.ModifyPrice(
                    It.Is<ModifyPriceInput>(input => input.Current == priceDetailsRule1)))
                .Returns(priceDetailsRule2);
            
            var rules = new List<IInvoiceRule>()
            {
                _mockRule.Object, _mockRule2.Object
            };
            
            var initialPrice = new PriceDetails
            {
                UnitPrice = 1,
                DiscountPrice = 2,
                DiscountVolume = 3,
            };
            
            const uint amount = 1;

            var result = rules.ModifyPriceDetails(initialPrice, amount);

            _mockRule.VerifyAll();
            _mockRule2.VerifyAll();
            
            result.Should().Be(priceDetailsRule2);
        }
        
        [Fact]
        public void ModifyTotal()
        {
            var totalRule1 = 123.45M;
            var totalRule2 = 234.56M;
            
            var sequence = new MockSequence();
            
            _mockRule
                .InSequence(sequence)
                .Setup(x => x.ModifyTotal(It.IsAny<ModifyTotalInput>()))
                .Returns(totalRule1);

            
            _mockRule2
                .InSequence(sequence)
                .Setup(x => x.ModifyTotal(
                    It.Is<ModifyTotalInput>(input => input.Current == totalRule1)))
                .Returns(totalRule2);
            
            var rules = new List<IInvoiceRule>()
            {
                _mockRule.Object, _mockRule2.Object
            };
            
            const decimal rawTotal = 998.76M;
            const decimal total = 678.91M;

            var result = rules.ModifyTotal(rawTotal, total);

            _mockRule.VerifyAll();
            _mockRule2.VerifyAll();
            
            result.Should().Be(totalRule2);
        }
    }
}