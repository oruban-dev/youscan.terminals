﻿using System;

namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public class UnitPriceDiscountDecorator : IInvoiceRule
    {
        private readonly IInvoiceRule _rule;

        private readonly IDiscount _discount;

        public int Order => _rule.Order;

        public UnitPriceDiscountDecorator(IDiscount discount, IInvoiceRule rule)
        {
            _rule = rule ?? throw new ArgumentNullException(nameof(rule));
            _discount = discount ?? throw new ArgumentNullException(nameof(discount));
        }

        public PriceDetails ModifyPrice(ModifyPriceInput input)
        {
            var result = _rule.ModifyPrice(input);
            result.UnitPrice = _discount.Apply(result.UnitPrice);

            return result;
        }

        public decimal ModifyTotal(ModifyTotalInput input)
        {
            return _rule.ModifyTotal(input);
        }
    }
}