﻿namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public interface ICardProvider
    {
        bool CanHandle(CardNumber cardNumber);

        ICard GetCardDetails(CardNumber cardNumber);

        CardNumber UpdateCardDetails(ICard card);
    }
}