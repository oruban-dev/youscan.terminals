﻿using System;
using FluentAssertions;
using Xunit;

namespace YouScan.Terminals.Abstractions.Tests
{
    public class InvoiceTests
    {
        [Fact]
        public void NewDefaultInvoiceIsUnsealed()
        {
            var invoice = new Invoice();
            invoice.IsSealed.Should().BeFalse();
        }

        [Fact]
        public void NewInvoiceWithTotalIsSealed()
        {
            var invoice = new Invoice(12345);
            invoice.IsSealed.Should().BeTrue();
        }

        [Fact]
        public void IsSealed_OK()
        {
            var invoice = new Invoice();
            invoice.Seal();
            invoice.IsSealed.Should().BeTrue();
        }

        [Fact]
        public void SealPreventTotalModification()
        {
            var invoice = new Invoice();
            invoice.Seal();

            Assert.Throws<InvalidOperationException>(() => invoice.Total = 12345);
        }
        
        [Fact]
        public void UnsealedInvoiceCouldBeModified()
        {
            var invoice = new Invoice();
            invoice.Total = 12345;
        }
        
        [Fact]
        public void SealedInvoice_SealNotThrow()
        {
            var invoice = new Invoice();
            invoice.Seal();
            invoice.Seal();
        }
    }
}