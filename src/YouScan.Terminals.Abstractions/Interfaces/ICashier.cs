﻿using System.Collections.Generic;

namespace YouScan.Terminals.Abstractions
{
    public interface ICashier<T, T2> 
        where T : IProductCode
        where T2 : IPriceDetails<T>
    {
        bool HasPrice(T productCode);
        
        void SetPrice(T2 priceDetails);
        
        Invoice CalculateInvoice(
            IReadOnlyDictionary<T, uint> products,
            IInvoiceGenerator<T, T2> invoiceGenerator);
        
        void Reset();
    }
}