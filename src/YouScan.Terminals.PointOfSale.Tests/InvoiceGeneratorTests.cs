using System.Collections.Generic;
using FluentAssertions;
using Moq;
using Xunit;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale.Tests
{
    public class InvoiceGeneratorTests
    {
        private readonly IReadOnlyDictionary<ProductCode, uint> _products;
        private readonly IReadOnlyDictionary<ProductCode, PriceDetails> _pricing;
        
        private readonly Mock<IValueCalculator> _calculator;
        private readonly InvoiceGenerator _invoiceGenerator;
            
        public InvoiceGeneratorTests()
        {
            _products = new Dictionary<ProductCode, uint>
            {
                { "A", 0},
                { "B", 1},
                { "C", 4},
            }; 
            
            _pricing = new Dictionary<ProductCode, PriceDetails>
            {
                { "A", new PriceDetails { ProductCode = "A"}},
                { "B", new PriceDetails { ProductCode = "B"}},
                { "C", new PriceDetails { ProductCode = "C"}},
            }; 
            
            _calculator = new Mock<IValueCalculator>();
            
            _invoiceGenerator = new InvoiceGenerator(_calculator.Object);
        }

        
        
        [Fact]
        public void DoNothingWhenEmpty()
        {
            var noProducts = new Dictionary<ProductCode, uint>();
            
            var result = _invoiceGenerator.Generate(_pricing, noProducts);
            
            _calculator.Verify(x => x.Calculate(It.IsAny<PriceDetails>(), It.IsAny<uint>()), Times.Never);
            _calculator.VerifyNoOtherCalls();
            
            result.Total.Should().Be(0);
        }
        
        [Fact]
        public void InvokeCalculationForCorrectProducts()
        {
            _calculator.SetReturnsDefault<decimal>(0);
            
            _invoiceGenerator.Generate(_pricing, _products);
            
            _calculator.Verify(x => x.Calculate(It.Is<PriceDetails>(details => details.ProductCode.Value == "A"), 0), Times.Once);
            _calculator.Verify(x => x.Calculate(It.Is<PriceDetails>(details => details.ProductCode.Value == "B"), 1), Times.Once);
            _calculator.Verify(x => x.Calculate(It.Is<PriceDetails>(details => details.ProductCode.Value == "C"), 4), Times.Once);
            
            _calculator.VerifyNoOtherCalls();
        }
        
        [Fact]
        public void TotalIsAggregated()
        {
            _calculator.Setup(x => x.Calculate(It.IsAny<PriceDetails>(), It.IsAny<uint>())).Returns(1.25M);
            
            var result = _invoiceGenerator.Generate(_pricing, _products);
            
            _calculator.Verify(x => x.Calculate(It.IsAny<PriceDetails>(), It.IsAny<uint>()), Times.Exactly(3));
            _calculator.VerifyNoOtherCalls();

            result.Total.Should().Be(3.75M);
        }
    }
}