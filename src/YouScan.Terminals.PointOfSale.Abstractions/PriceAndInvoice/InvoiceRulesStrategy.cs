﻿using System.Collections.Generic;
using System.Linq;

namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public static class InvoiceRulesStrategy
    {
        public static PriceDetails ModifyPriceDetails(
            this IEnumerable<IInvoiceRule> rules,
            PriceDetails initialPrice,
            uint amount)
        {
            var modifyPriceInput = new ModifyPriceInput
            {
                Initial = initialPrice,
                Current = new PriceDetails
                {
                    ProductCode = initialPrice.ProductCode,
                    UnitPrice = initialPrice.UnitPrice,
                    DiscountPrice = initialPrice.DiscountPrice,
                    DiscountVolume = initialPrice.DiscountVolume
                },
                Amount = amount
            };

            foreach (var rule in rules.OrderByDescending(x => x.Order))
            {
                modifyPriceInput.Current = rule.ModifyPrice(modifyPriceInput);
            }

            return modifyPriceInput.Current;
        }
        
        public static decimal ModifyTotal(
            this IEnumerable<IInvoiceRule> rules,
            decimal rawTotal,
            decimal total)
        {
            var modifyTotalInput = new ModifyTotalInput
            {
                Initial = rawTotal,
                Current = total,
            };

            foreach (var rule in rules.OrderByDescending(x => x.Order))
            {
                modifyTotalInput.Current = rule.ModifyTotal(modifyTotalInput);
            }

            return modifyTotalInput.Current;
        }
    }
}