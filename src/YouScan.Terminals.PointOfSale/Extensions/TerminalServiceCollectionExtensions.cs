﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale.Extensions
{
    public static class TerminalServiceCollectionExtensions
    {
        public static IServiceCollection AddPointOfSaleTerminal(this IServiceCollection services)
        {
            services.AddInvoiceGenerator();

            services.TryAddTransient<ICashier<ProductCode, PriceDetails>, Cashier>();

            services.TryAddTransient<IProductLine<ProductCode>, ProductScanner>();

            services.TryAddTransient<ITerminal<ProductCode, PriceDetails>, PointOfSaleTerminal>();

            return services;
        }

        public static IServiceCollection AddTerminalWithCards(
            this IServiceCollection services, 
            Func<IServiceProvider, ICardProviderFactory> cardProviderFactoryCreator)
        {                
            services.AddInvoiceGenerator();

            services.TryAddTransient<ICashierWithInvoiceRules, CashierWithInvoiceRules>();

            services.TryAddTransient<IProductLine<ProductCode>, ProductScanner>();

            services.TryAddTransient(cardProviderFactoryCreator);
            
            services.TryAddTransient<ICardProvider, CardModule>();

            services.TryAddTransient<IPointOfSaleTerminalWithCards, PointOfSaleTerminalWithCards>();

            return services;
        }

        public static IServiceCollection AddInvoiceGenerator(this IServiceCollection services)
        {
            services.TryAddSingleton<IValueCalculator, DiscountCalculator>();

            services.TryAddSingleton<IInvoiceGenerator<ProductCode, PriceDetails>, InvoiceGenerator>();

            return services;
        }
    }
}