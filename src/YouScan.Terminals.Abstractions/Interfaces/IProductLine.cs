﻿using System.Collections.Generic;

namespace YouScan.Terminals.Abstractions
{
    public interface IProductLine<T>
        where T : IProductCode
    {
        void Scan(T productCode);
        IReadOnlyDictionary<T, uint> GetProducts();
        void ResetLine();
    }
}