﻿namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public interface IInvoiceRule
    {
        int Order { get; }

        PriceDetails ModifyPrice(ModifyPriceInput input);

        decimal ModifyTotal(ModifyTotalInput input);
    }

    public class ModifyPriceInput
    {
        public PriceDetails Initial { get; set; }
        public PriceDetails Current { get; set; }
        public uint Amount { get; set; }
    }

    public class ModifyTotalInput
    {
        public decimal Initial { get; set; }
        public decimal Current { get; set; }
        public uint Amount { get; set; }
    }
}