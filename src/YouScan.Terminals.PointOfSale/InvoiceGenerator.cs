﻿using System.Collections.Generic;
using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    public class InvoiceGenerator : IInvoiceGenerator<ProductCode, PriceDetails>
    {
        private readonly IValueCalculator _calculator;

        public InvoiceGenerator(IValueCalculator calculator)
        {
            _calculator = calculator;
        }
        
        public Invoice Generate(IReadOnlyDictionary<ProductCode, PriceDetails> pricing, IReadOnlyDictionary<ProductCode, uint> products)
        {
            var total = 0M;

            foreach (var product in products)
            {
                var priceDetails = pricing[product.Key];
                var amount = product.Value;

                total += _calculator.Calculate(priceDetails, amount);
            }

            return new Invoice { Total = total };
        }
    }
}