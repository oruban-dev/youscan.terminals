﻿namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public interface ICard
    {
        CardNumber CardNumber { get; set; }
    }
}