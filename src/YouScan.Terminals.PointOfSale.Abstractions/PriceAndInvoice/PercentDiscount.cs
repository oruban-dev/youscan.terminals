﻿using System;

namespace YouScan.Terminals.PointOfSale.Abstractions
{
    public class PercentDiscount : IDiscount
    {
        public decimal Value { get; }

        public PercentDiscount(decimal value)
        {
            if (value > 1 || value < 0)
            {
                throw new ArgumentException("value should be between 0 and 1");
            }

            Value = value;
        }

        public decimal Apply(decimal price)
        {
            return price * (1 - Value);
        }
    }
}