﻿using YouScan.Terminals.Abstractions;
using YouScan.Terminals.PointOfSale.Abstractions;

namespace YouScan.Terminals.PointOfSale
{
    public interface IPointOfSaleTerminalWithCards : 
        ITerminal<ProductCode, PriceDetails>,
        ICardScanner
    {
        void ApplyPayment();
    }
}