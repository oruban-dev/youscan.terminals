﻿using System;
using Moq;
using Xunit;

namespace YouScan.Terminals.Abstractions.Tests
{
    public class TerminalTests
    {
        private readonly Terminal<IProductCode, IPriceDetails<IProductCode>> _testTerminal;
        private readonly Mock<IProductLine<IProductCode>> _mockScanner;
        private readonly Mock<ICashier<IProductCode, IPriceDetails<IProductCode>>> _mockCashier;

        public TerminalTests()
        {
            _mockScanner = new Mock<IProductLine<IProductCode>>();
            _mockCashier = new Mock<ICashier<IProductCode, IPriceDetails<IProductCode>>>();
            var invoiceGenerator = new Mock<IInvoiceGenerator<IProductCode, IPriceDetails<IProductCode>>>();
                
            _testTerminal = 
                new Terminal<IProductCode, IPriceDetails<IProductCode>>(
                    _mockScanner.Object,
                    _mockCashier.Object,
                    invoiceGenerator.Object);

        }
        
        #region Product Scan
        [Fact]
        public void Scan()
        {
            var productCode = new Mock<IProductCode>().Object;
            
            _mockCashier
                .Setup(x => x.HasPrice(productCode))
                .Returns(true);
            
            _testTerminal.Scan(productCode);
            
            
            _mockCashier.Verify(x => x.HasPrice(productCode), Times.Once);
            _mockScanner.Verify(x => x.Scan(productCode), Times.Once);
            
            _mockCashier.VerifyNoOtherCalls();
            _mockScanner.VerifyNoOtherCalls();
        }
        
        [Fact]
        public void ScanUnknownProduct()
        {
            var productCode = new Mock<IProductCode>().Object;
            
            _mockCashier
                .Setup(x => x.HasPrice(It.IsNotNull<IProductCode>()))
                .Returns(false);
            
            Assert.Throws<InvalidOperationException>( 
                () => _testTerminal.Scan(productCode)
                );
            
            _mockCashier.Verify(x => x.HasPrice(It.IsAny<IProductCode>()), Times.Once);
            
            _mockCashier.VerifyNoOtherCalls();
            _mockScanner.VerifyNoOtherCalls();
        }
        
        #endregion Product Scan

        [Fact]
        public void Reset()
        {
            _testTerminal.Reset();
            
            _mockCashier.Verify(x => x.Reset(), Times.Once);
            _mockScanner.Verify(x => x.ResetLine(), Times.Once);
        }
        
        [Fact]
        public void ResetLine()
        {
            _testTerminal.ResetLine();
            
            _mockCashier.Verify(x => x.Reset(), Times.Never);
            _mockScanner.Verify(x => x.ResetLine(), Times.Once);
        }
    }
    
}