﻿using System.Collections.Generic;

namespace YouScan.Terminals.Abstractions
{
    public interface IInvoiceGenerator<T, T2>
        where T : IProductCode
        where T2 : IPriceDetails<T>
    {
        Invoice Generate(
            IReadOnlyDictionary<T, T2> pricing,
            IReadOnlyDictionary<T, uint> products);
        
    }
}